<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170115_112241_AddTableTags extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('taxonomy_tag', [
            'id'         => $this->primaryKey(11),
            'slug'       => $this->string(255)->notNull(),
            'name'       => $this->string(255)->notNull(),
            'frequency'  => $this->integer(11),
            'status'     => $this->integer(11),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_taxonomy_tag_created_by',
            'taxonomy_tag',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_taxonomy_tag_updated_by',
            'taxonomy_tag',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createIndex('taxonomy_tag_frequency', 'taxonomy_tag', 'frequency');
        $this->createIndex('taxonomy_tag_name', 'taxonomy_tag', 'name');


        $this->createTable('taxonomy_tag_link', [
            'id'         => $this->primaryKey(11),
            'object'     => $this->string(255)->notNull(),
            'object_id'  => $this->integer(11),
            'tag_id'     => $this->integer(11),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_taxonomy_tag_link_tag',
            'taxonomy_tag_link',
            'tag_id',
            'taxonomy_tag',
            'id'
        );

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('taxonomy_tag');
        $this->dropTable('taxonomy_tag_link');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
