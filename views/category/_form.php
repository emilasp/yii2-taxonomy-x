<?php

use emilasp\json\widgets\DynamicFields\DynamicFields;
use emilasp\seo\widgets\SeoForm\SeoForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="category-form">

        <?php $form = ActiveForm::begin([
            'id' => 'category-form',
        ]); ?>

        <ul class="nav nav-tabs">
            <li>
                <a data-toggle="tab" href="#base" class="nav-link active">
                    <?= Yii::t('site', 'Tab Base') ?>
                </a>
            </li>
            <li><a data-toggle="tab" href="#seo" class
                ="nav-link"><?= Yii::t('site', 'Tab Seo') ?></a></li>
        </ul>

        <div class="tab-content">
            <div id="base" class="tab-pane active">
                <?php /* $form->field($model, 'type')->dropDownList($model::$types) */ ?>
                <?= $form->field($model, 'parent')->hiddenInput(['id' => 'cat_parent_id'])->label(false) ?>
                <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                    'options' => ['rows' => 3],
                    'preset'  => 'standart',
                ]) ?>
                <?= $form->field($model, 'short_text')->widget(CKEditor::className(), [
                    'options' => ['rows' => 3],
                    'preset'  => 'standart',
                ]) ?>


                <?php //@TODO добавить загрузку изображения ?>
                <?= $form->field($model, 'image_id')->textInput() ?>
                <?php
                $status = $model->statuses;
                ?>
                <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
                <?= $form->field($model, 'type')->dropDownList($model::$types) ?>
            </div>
            <div id="seo" class="tab-pane fade">
                <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
            </div>
        </div>


        <div class="text-right">
            <?= Html::button($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Delete'), [
                'id'      => 'category-delete',
                'data-id' => $model->id,
                'class'   => 'btn btn-danger'
            ]) ?>
            <?= Html::submitButton(
                $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
                ['class' => 'btn btn-success']
            ) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
