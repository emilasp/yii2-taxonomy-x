<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\Category */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('taxonomy', 'Type Category'),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
