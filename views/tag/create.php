<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\Tag */

$this->title = Yii::t('taxonomy', 'Create Tag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
