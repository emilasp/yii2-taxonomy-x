<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\Tag */

$this->title = Yii::t('taxonomy', 'Update {modelClass}: ', [
    'modelClass' => 'Tag',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('taxonomy', 'Update');
?>
<div class="tag-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
