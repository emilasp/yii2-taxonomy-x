<?php
namespace emilasp\taxonomy\behaviors;

use emilasp\core\helpers\StringHelper;
use emilasp\taxonomy\models\Tag;
use emilasp\taxonomy\models\TagLink;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Поведение добавляет модели тегирование
 *
 * Добавляем поведение:
 * ```php
 * 'tags'  => [
 *     'class' => TagBehavior::className(),
 * ],
 * ```
 *
 * В правила валидации добавляем formTags аттрибут:
 * ```php
 * public function rules()
 * {
 *     return [
 *         ['formTags', 'safe'],
 *     ];
 * }
 * ```
 * В модель добавляем relations:
 * ```php
 * public function getTagLink()
 * {
 *     return $this->hasMany(TagLink::className(), ['object_id' => 'id'])
 *         ->andOnCondition(['object' => self::className()]);
 * }
 * public function getTags()
 * {
 *     return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
 *         ->via('tagLink');
 * }
 * ```
 *
 * В экшен update добавляем установку тегов свойству formTags
 * ```php
 * $model->setTags();
 * if ($model->load(Yii::$app->request->post()) && $model->save()) {
 * $model->saveTags();
 * ...
 *  ```
 *
 * Добавляем виджет с тегами на форму:
 * ```php
 * $form->field($model, 'formTags')->widget(SelectizeTextInput::className(), [
 *     'loadUrl' => ['/taxonomy/tag/search'],
 *     'options' => ['class' => 'form-control'],
 *     'clientOptions' => [
 *         'plugins' => ['remove_button', 'drag_drop', 'restore_on_backspace'],
 *         'valueField' => 'name',
 *         'labelField' => 'name',
 *         'searchField' => ['name'],
 *         'create' => true,
 *     ],
 * ])->hint('Используйте запятые для разделения меток')
 * ```
 *
 * Class TagBehavior
 * @package emilasp\taxonomy\behaviors
 */
class TagBehavior extends Behavior
{
    public $formTags;

    public $modelName = 'emilasp\taxonomy\models\Tag';

    public $attribute = 'formTags';
    public $relation = 'tags';

    /*public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'saveTags',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveTags',
        ];
    }*/

    /**
     * Сохраняем теги для модели
     */
    public function saveTags()
    {
        $model = $this->owner;
        $tags  = $this->formTags;

        $tags = explode(',', $tags);

        $issetTags = [];
        foreach ($tags as $tag) {
            if ($tag) {
                $tagInBd                 = $this->getTag($tag);
                $issetTags[$tagInBd->id] = $tagInBd;
            }
        }

        foreach ($model->{$this->relation} as $issetTag) {
            if (!isset($issetTags[$issetTag->id])) {
                $linkToDelete = TagLink::findOne([
                    'object'    => $model::className(),
                    'object_id' => $model->id,
                    'tag_id'    => $issetTag->id,
                ]);

                if ($linkToDelete) {
                    $linkToDelete->delete();
                }
            }
        }
    }

    /**
     * Получаем tag
     *
     * @param $tag
     * @return Tag|null
     */
    private function getTag($tag)
    {
        $model = $this->owner;
        $tagModelName = $this->modelName;

        $tagInBd = null;
        foreach ($model->{$this->relation} as $issetTag) {
            if ($issetTag->name === $tag) {
                $tagInBd = $issetTag;
                break;
            }
        }

        if (!$tagInBd) {
            $tagInBd = $tagModelName::findOne(['name' => $tag]);

            if (!$tagInBd) {
                $tagInBd = new $tagModelName(['name' => $tag, 'slug' => StringHelper::str2url($tag), 'status' => 1]);
                $tagInBd->save();
            }

            $tagLink            = new TagLink();
            $tagLink->object    = $model::className();
            $tagLink->object_id = $model->id;
            $tagLink->tag_id    = $tagInBd->id;
            $tagLink->save();
        }

        return $tagInBd;
    }

    /**
     * Устанавливаем теги для модели
     */
    public function setTags()
    {
        $tags = $this->owner->tags;

        if ($tags) {
            $this->formTags = implode(',', ArrayHelper::map($tags, 'id', 'name'));
        }
    }
}
