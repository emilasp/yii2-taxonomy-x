<?php
return [
    'Taxonomy'      => 'Таксономия',
    'Taxonomies'    => 'Таксономии',
    'Group'         => 'Группа',
    'Groups'        => 'Группы',
    'Value'         => 'Значение',
    'Data'          => 'Данные',
    'Variated'      => 'Вариантный',
    'Variation'     => 'Вариация',
    'Type Category' => 'Тип категории',
    'Category'      => 'Категория',
    'Categories'    => 'Категории',
    'Tag'           => 'Тег',
    'Tags'          => 'Теги',
    'Frequency'     => 'Частота',
    'Slug'          => 'Slug',

    'Create Tag'       => 'Создать тег',
    'Creating Tag'     => 'Создание тега',
    'Create Category'  => 'Создание категории',
    'Count categories' => 'Количество категорий',
];
