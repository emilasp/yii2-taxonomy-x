<?php

namespace emilasp\taxonomy\models;

use Yii;
use emilasp\core\components\base\ActiveRecord;

/**
 * This is the model class for table "taxonomy_tag_link".
 *
 * @property integer $id
 * @property string  $object
 * @property integer $object_id
 * @property integer $tag_id
 * @property string  $updated_at
 *
 * @property Tag     $tag
 */
class TagLink extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxonomy_tag_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object'], 'required'],
            [['object_id', 'tag_id'], 'integer'],
            [['updated_at'], 'safe'],
            [['object'], 'string', 'max' => 255],
            [
                ['tag_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tag::className(),
                'targetAttribute' => ['tag_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('taxonomy', 'ID'),
            'object'     => Yii::t('taxonomy', 'Object'),
            'object_id'  => Yii::t('taxonomy', 'Object ID'),
            'tag_id'     => Yii::t('taxonomy', 'Tag ID'),
            'updated_at' => Yii::t('taxonomy', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['id' => 'tag_id']);
    }
}
