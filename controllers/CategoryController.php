<?php

namespace emilasp\taxonomy\controllers;

use emilasp\core\helpers\StringHelper;
use Yii;
use emilasp\taxonomy\models\Category;
use emilasp\taxonomy\models\search\CategorySearch;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => [
                    'index',
                    'view',
                    'create',
                    'update',
                    'delete',
                    'moveNode',
                    'deleteNode',
                    'updateNode',
                    'createNode',
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'createNode',
                            'moveNode',
                            'updateNode',
                            'deleteNode',
                        ],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                //'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex(int $rootId, string $className = null)
    {
        $className = $className ?: Category::className();

        $root = $className::findOne($rootId);

        $modelId = Yii::$app->request->get('modelId', null);

        if ($modelId) {
            $model = $className::findOne($modelId);
        } else {
            $model = new $className();
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->id) {
                $isSave = $model->update();
                //$model->save();
            } else {
                $isSave = $model->appendTo($root);
            }


            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($isSave) {
                return [1, 'Успешно'];
            } else {
                return [0, 'Не удалось сохранить'];
            }
        }

        $searchModel  = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'model'        => $model,
            'root'         => $root,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @return mixed
     */
    public function actionDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');
        $className = Yii::$app->request->post('categoryClass', 'emilasp\taxonomy\models\Category');

        $model         = $this->findModel($id, $className::className());
        $model->status = Category::STATUS_DELETED;

        if ($model->created_by === Yii::$app->user->id && $model->save(false)) {
            $return[] = 1;
            $return[] = 'Успешно удалено';
        } else {
            $return[] = 0;
            $return[] = 'Не удалось удалить';
        }

        return $return;
    }

    /**
     * Сохраняем перемещения нод
     */
    public function actionMoveNode()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $node       = (int)Yii::$app->request->post('node', null);
        $nodeName   = Yii::$app->request->post('name', null);
        $nodeTarget = (int)Yii::$app->request->post('nodeTarget', null);
        $mode       = Yii::$app->request->post('mode', null);
        $className  = Yii::$app->request->post('categoryClass', 'emilasp\taxonomy\models\Category');

        /** @var Category $category */
        $category       = $className::findOne($node);
        $categoryTarget = $className::findOne($nodeTarget);

        switch ($mode) {
            case 'before':
                $category->insertBefore($categoryTarget);
                break;
            case 'after':
                $category->insertAfter($categoryTarget);
                break;
            case 'over':
                $category->appendTo($categoryTarget);
                break;
        }

        if ($category->hasErrors()) {
            $return[] = 0;
            $return[] = 'Шибка сохранения';
        } else {
            $return[] = 1;
            $return[] = 'Успешно';
        }

        return $return;
    }


    /**
     * Сохраняем изменение ноды
     */
    public function actionUpdateNode()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $node      = (int)Yii::$app->request->post('node', null);
        $nodeName  = Yii::$app->request->post('name', null);
        $className = Yii::$app->request->post('categoryClass', 'emilasp\taxonomy\models\Category');

        /** @var Category $category */
        $category = $className::findOne($node);

        if ($category) {
            $category->code = StringHelper::str2url($nodeName);
            $category->name = $nodeName;
            $category->save();
        }

        if ($category->hasErrors()) {
            $status  = 0;
            $message = 'Шибка сохранения';
        } else {
            $status  = 1;
            $message = 'Успешно';
        }

        return $this->setAjaxResponse($status, $message, $params = ['id' => $category->id]);
    }


    /**
     * Создаем ноду
     */
    public function actionCreateNode()
    {
        $node       = Yii::$app->request->post('node', null);
        $nodeTarget = Yii::$app->request->post('nodeTarget', null);
        $mode       = Yii::$app->request->post('mode', null);

        $newNodeName = Yii::$app->request->post('name', null);
        $className   = Yii::$app->request->post('categoryClass', null);

        /** @var Category $category */
        $categoryTarget = $className::findOne($nodeTarget);
        $category       = $className::findOne($node);


        if (!$category) {
            $category             = new $className;
            $category->code       = StringHelper::str2url($newNodeName);
            $category->name       = $newNodeName;
            $category->text       = $newNodeName;
            $category->short_text = $newNodeName;
            $category->type       = Category::TYPE_CATEGORY;
            $category->status     = Category::STATUS_ENABLED;
            //$category->save();
        }

        if (!$category->depth) {
            $mode = 'over';
        }

        switch ($mode) {
            case 'before':
                $category->insertBefore($categoryTarget);
                break;
            case 'after':
                $category->insertAfter($categoryTarget);
                break;
            case 'over':
                $category->appendTo($categoryTarget);
                break;
        }

        if ($category->hasErrors()) {
            $status  = 0;
            $message = 'Шибка сохранения';
        } else {
            $status  = 1;
            $message = 'Успешно';
        }

        return $this->setAjaxResponse($status, $message, $params = ['id' => $category->id]);
    }
}
