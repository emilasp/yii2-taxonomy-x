<?php
namespace emilasp\taxonomy\extensions\CategorySelectTree;

use rmrevin\yii\minify\components\JS;
use Yii;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\InputWidget;
use yii\helpers\ArrayHelper;
use emilasp\taxonomy\extensions\nestedTree\NestedTree;

/**
 * Class CategorySelectTree
 * @package emilasp\taxonomy\extensions\CategorySelectTree
 */
class CategorySelectTree extends InputWidget
{
    /** @var  string classname category model */
    public $categoryClassName;

    /** @var string рутовый итем */
    public $id = 'tree';

    /** @var string рутовый итем */
    public $rootId;

    public $fieldId = 'category';

    public $options = [];

    /** @var array */
    private $defaultOptions = [
        'id'             => 'tree',
        'extensions'     => ['glyph', 'dnd', 'edit'],
        'glyph'          => '{
                            map: {
                              doc: "glyphicon glyphicon-file",
                              docOpen: "glyphicon glyphicon-file",
                              checkbox: "glyphicon glyphicon-unchecked",
                              checkboxSelected: "glyphicon glyphicon-check",
                              checkboxUnknown: "glyphicon glyphicon-share",
                              dragHelper: "glyphicon glyphicon-play",
                              dropMarker: "glyphicon glyphicon-arrow-right",
                              error: "glyphicon glyphicon-warning-sign",
                              expanderClosed: "glyphicon glyphicon-plus-sign",
                              expanderLazy: "glyphicon glyphicon-plus-sign",
                              expanderOpen: "glyphicon glyphicon-minus-sign",
                              folder: "glyphicon glyphicon-folder-close",
                              folderOpen: "glyphicon glyphicon-folder-open",
                              loading: "glyphicon glyphicon-refresh"
                            }
                          }',
        'checkbox'       => true,
        'selectMode'     => 2,
        'minExpandLevel' => 3,
        'init'           => 'function(event) {
                var val = $("#{{selectorFiled}}-input").val();
                
                var ids = [];
                
                if (val !== "") {
                    if (!isNaN(val)) {
                        ids = [val];
                    } else {
                        ids = JSON.parse(val);
                    }
                }
                
                $(event.target).on("mouseover", ".fancytree-title", function(event){
                    var node = $.ui.fancytree.getNode(event);
                    var butDel = " <span class=\"ft-button-del\" data-id=\"" + node.key + "\">-</button> ";

                    if (node.key.indexOf("_") === -1 && node.title.indexOf(butDel) === -1) {
                        node.setTitle(node.title + butDel);
                    }
                });
                
                $(\'body\').on(\'click\', \'.ft-button-del\', function (event) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    
                    if (confirm("Delete item?")) {
                        var node = $.ui.fancytree.getNode(event);
                        var categoryClass = "\\\emilasp\\\library\\\models\\\Folder";
                        
                        $.ajax({
                            type: "POST",
                            url: "/taxonomy/category/delete.html",
                            dataType: "json",
                            data: "categoryClass=" + categoryClass + "&node=" + node.key,
                            success: function(msg) {
                                if(msg[0]=="1"){
                                    node.remove();
                                }else{
                                    notice(msg[1], "red");
                                }
                            },
                            error: function(){}
                        });
                    }
                });  
                
                $(event.target).fancytree("getTree").visit(function(node){
                    var checked = false;
                    $.map(ids, function(element, index) {
                       if (element == node.key) {
                       console.log(element);
                           checked = true;
                           node.toggleSelected();
                           node.setActive();
                       }
                    });
                });
            }',
        'select'         => 'function(event, data) {
                                var selNodes = data.tree.getSelectedNodes();
                                var selKeys = $.map(selNodes, function(node){return node.key;});
                                var category_id = "";
                                if ($.isArray(selKeys) && selKeys.length) {
                                    $("#{{selectorFiled}}-input").val("[" + selKeys.join(", ") + "]");
                                }

                                //console.log($("#{{selectorFiled}}-input").val());
                            }',
        'click'          => 'function(event, data) {
                if(!data.node.folder){
                    data.node.toggleSelected();
                }
            }',
        /*'dblclick'       => 'function(event, data) {
                data.node.toggleExpanded();
                console.log("dblclick");
            }',*/

        'edit'    => [
            'adjustWidthOfs' => 4,   // null: don't adjust input size to content
            'inputCss'       => '{ minWidth: "3em" }',
            'triggerStart'   => ["f2", "dblclick", "shift+click", "mac+enter"],
            /* 'beforeEdit'     => '$.noop',// Return false to prevent edit mode
             'edit'           => '$.noop',// Editor was opened (available as data.input)
             'beforeClose'    => '$.noop',// Return false to prevent cancel/save (data.input is available)
             'save'           => '$.noop',// Save data.input.val() or return false to keep editor open
             'close'          => '$.noop',// Editor was removed*/
            'save'           => 'function(event, data){
                return true;
            }',
            'close'          => 'function(event, data){
                var categoryClass = "{categoryClass}";
                var node = data.node;
                
                  $.ajax({
                      type: "POST",
                      url: "/taxonomy/category/update-node.html",
                      dataType: "json",
                      data: "categoryClass=" + categoryClass + "&name="+node.title+ "&node="+node.key,
                      success: function(msg) {
                          if(msg[0]=="1"){
                              notice(msg[1], "green");
                          }else{
                              notice(msg[1], "red");
                          }
                      },
                      error: function(){}
                  });
                
                return true;
            }',
        ],
        'keydown' => 'function(event, data) {
                if( event.which === 32 ) {
                    data.node.toggleSelected();
                    return false;
                }
            }',
        'dnd'     => [
            'preventVoidMoves'      => true,
            'preventRecursiveMoves' => true,
            'autoExpandMS'          => 400,
            'dragStart'             => 'function(node, data) {
                                          return true;
                                      }',
            'dragEnter'             => 'function(node, data) {
                                       return true;
                                    }',
            'dragDrop'              => 'function(node, data) {
                             
                              var categoryClass = "{categoryClass}";
                            
                              $.ajax({
                                  type: "POST",
                                  url: "/taxonomy/category/move-node.html",
                                  dataType: "json",
                                  data: "categoryClass=" + categoryClass + "&name="+data.otherNode.title+ "&node="+data.otherNode.key+"&nodeTarget="+data.node.key+"&mode="+data.hitMode,
                                  success: function(msg) {
                                      if(msg[0]=="1"){
                                          data.otherNode.moveTo(node, data.hitMode);
                                      }else{
                                          notice(msg[1], "red");
                                      }
                                  },
                                  error: function(){}
                              });
                          }',
        ],
    ];

    /** @var array события fancyTree */
    public static $events = ['init', 'select', 'click', 'dblclick', 'keydown', 'glyph'];

    public function init()
    {
        parent::init();
        $this->prepareOptions();
        $this->registerAssest();
    }

    public function run()
    {
        $this->renderAddCategoryInput();
        echo NestedTree::widget([
            'id'                => $this->id,
            'categoryClassName' => $this->categoryClassName,
            'rootId'            => $this->rootId,
            'options'           => $this->defaultOptions,
        ]);
        echo Html::activeInput('hidden', $this->model, $this->attribute, ['id' => $this->id . '-input']);
    }

    /**
     * Формируем инпут для создани новой категории
     */
    private function renderAddCategoryInput()
    {
        echo Html::beginTag('div', ['class' => 'input-group']);
        echo Html::textInput('Category-add', null, [
            'class'       => 'add-category form-control',
            'placeholder' => Yii::t('taxonomy', 'New category')
        ]);
        echo Html::beginTag('span', ['class' => 'input-group-btn']);
        echo Html::button(Yii::t('site', 'Create'), ['class' => 'btn btn-secondary btn-add-category']);
        echo Html::endTag('span');
        echo Html::endTag('div');
    }

    /**
     * Оборачиваем JS и устанавливаем в него id selector для инпута
     */
    private function prepareOptions()
    {
        $this->rootId = ($this->categoryClassName)::ROOT_CATEGORY;

        $this->defaultOptions = ArrayHelper::merge($this->defaultOptions, $this->options);
        foreach ($this->defaultOptions as $index => $option) {
            if (in_array($index, self::$events)) {
                $option                       = str_replace('{{selectorFiled}}', $this->id, $option);
                $this->defaultOptions[$index] = new JsExpression($option);
            }
        }

        /** DND events */
        if (isset($this->defaultOptions['dnd']['dragDrop'])) {
            $this->defaultOptions['dnd']['dragDrop']  = str_replace(
                '{categoryClass}',
                str_replace('\\', '\\\\', $this->categoryClassName),
                $this->defaultOptions['dnd']['dragDrop']
            );
            $this->defaultOptions['dnd']['dragDrop']  = new JsExpression($this->defaultOptions['dnd']['dragDrop']);
            $this->defaultOptions['dnd']['dragStart'] = new JsExpression($this->defaultOptions['dnd']['dragStart']);
            $this->defaultOptions['dnd']['dragEnter'] = new JsExpression($this->defaultOptions['dnd']['dragEnter']);
        }

        if (isset($this->defaultOptions['edit']['close'])) {
            $this->defaultOptions['edit']['close'] = str_replace(
                '{categoryClass}',
                str_replace('\\', '\\\\', $this->categoryClassName),
                $this->defaultOptions['edit']['close']
            );
            $this->defaultOptions['edit']['save']  = new JsExpression($this->defaultOptions['edit']['save']);
            $this->defaultOptions['edit']['close'] = new JsExpression($this->defaultOptions['edit']['close']);
        }
    }

    private function registerAssest()
    {
        $inputId = Html::getInputId($this->model, $this->attribute);

        $class = str_replace('\\', '\\\\', $this->categoryClassName);
        $js    = <<<JS
        $('body').on('click', '.btn-add-category', function (event) {
            var name = $('.add-category').val();
            
            if (name) {
                var tree = $("#{$inputId}").fancytree("getTree");
                var node = tree.getActiveNode();
                                
                if (node) {
                    var mode = 'child';
                                        
                    if (!node.parent.parent) {
                        mode = 'over';
                    }
                    
                    $.ajax({
                        type: "POST",
                        url: "/taxonomy/category/create-node.html",
                        dataType: "json",
                        data: "categoryClass={$class}&name="+name+ "&nodeTarget="+node.key + "&mode=" + mode,
                        success: function(msg) {                          
                            if(msg.status=='1'){
                                var newNode;
                                if (mode === 'after') {
                                    newNode = node.addNode({key:msg.data.id, title: name}, 'after');
                                } else {
                                    newNode = node.addNode({key:msg.data.id, title: name}, 'child');
                                }
                                 
                                 newNode.setActive();
                                 $("#tree-input").val(msg.data.id);
                                 notice(msg.message, "green");
                            } else {
                                notice(msg.message, "red");
                            }
                        },
                        error: function(){}
                    });
                } else {
                    alert('Select Parent Category');
                }         
            }
        });
JS;

        $this->view->registerJs($js);
    }
}
